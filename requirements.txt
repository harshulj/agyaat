Django==1.6.2
South==0.8.4
argparse==1.2.1
django-debug-toolbar==1.0.1
sqlparse==0.1.11
wsgiref==0.1.2
