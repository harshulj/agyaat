from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from models import AppUser

@login_required()
def account( request, template='account/user.html'):
    loggedin_user = request.user
    return render(request, template, { 'loggedin_user': loggedin_user })

@login_required()
def user( request, user_id, template='account/user.html'):
    user = get_object_or_404(AppUser, pk=user_id)
    loggedin_user = request.user
    return render(request, template, {'loggedin_user': loggedin_user, 'user': user})
