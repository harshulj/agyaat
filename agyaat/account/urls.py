from django.conf.urls import patterns, include, url
from views import account, user

urlpatterns = patterns('',
    url(r'^$', account, name='account'),
    url(r'^', include('registration.backends.agyaat.urls')),
    url(r'^(?P<user_id>\d+)/$', user, name='user'),
    url(r'^', include('relationships.urls')),
)
