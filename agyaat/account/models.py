from django.db import models
from django.core.mail import send_mail
from django.utils import timezone
from django.utils.http import urlquote
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager,PermissionsMixin


class AppUserManager(BaseUserManager):
    def _create_user(self, email, profession, place, password, is_staff, is_superuser, **extra_fields):
        now = timezone.now()
        if not email:
            raise ValueError("Email address is necessary.")
        if not profession:
            raise ValueError("Profession is necessary.")
        if not place:
            raise ValueError("Place is necessary.")
        email = self.normalize_email(email)
        user = self.model(email=email, profession=profession, place=place, is_staff=is_staff, is_superuser=is_superuser, last_login=now, date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, profession, place, password=None, **extra_fields):
        return self._create_user(email, profession, place, password, False, False, **extra_fields)

    def create_superuser(self, email, profession, place, password=None, **extra_fields):
        return self._create_user(email, profession, place, password, True, True, **extra_fields)


class AppUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(max_length=50, unique=True, db_index=True)
    profession = models.CharField(max_length=255)
    place = models.CharField(max_length=255)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    objects = AppUserManager()


    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['place', 'profession']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_absolute_url(self):
        return "/accounts/%s/" % urlquote(self.id)

    def get_full_name(self):
        return '%s from %s' %(self.profession, self.place)

    def get_short_name(self):
        return 'A %s' % self.profession

    def email_user(self, subject, message, from_email=None):
        send_mail(subject, message, from_email, [self.email])
