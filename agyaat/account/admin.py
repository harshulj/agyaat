from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from account.models import AppUser
from account.forms import AppUserChangeForm, AppUserCreationForm


class AppUserAdmin(UserAdmin):
    fieldsets = (
            (None, {'fields': ('email', 'password')}),
            (_('Personal info'), {'fields': ('profession', 'place')}),
            (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                'groups', 'user_permissions')}),
            #(_('Important dates'), {'fields': ('last_login')}),
            )
    add_fieldsets = (
            (None, {
                'classes': ('wide',),
                'fields': ('email', 'profession', 'place','password1', 'password2')}
                ),
            )
    form = AppUserChangeForm
    add_form = AppUserCreationForm
    list_display = ('email','profession', 'place', 'date_joined', 'is_active')
    search_fields = ('email', 'profession', 'place')
    ordering = ('date_joined', 'email',)

admin.site.register(AppUser, AppUserAdmin)
